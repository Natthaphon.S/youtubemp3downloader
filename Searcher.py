import os


from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
"""
CLIENT_SECRETS_FILE = "client_secret.json"
SCOPES = ['https://www.googleapis.com/auth/youtube.force-ssl']
API_SERVICE_NAME = 'youtube'
API_VERSION = 'v3'
"""
class Search():
    def __init__(self,CLIENT_SECRETS_FILE,SCOPES,API_SERVICE_NAME,API_VERSION):
        self.cliSecret = CLIENT_SECRETS_FILE
        self.scope = SCOPES
        self.apiname = API_SERVICE_NAME
        self.apiver = API_VERSION
        os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = '1'
        self.client = self.get_authenticated_service()
    def get_authenticated_service(self):
        self.flow = InstalledAppFlow.from_client_secrets_file(self.cliSecret, self.scope)
        self.credentials = self.flow.run_local_server(openbrowser=False)
        return build(self.apiname, self.apiver, credentials=self.credentials, )

    def remove_empty_kwargs(self,**kwargs):
        good_kwargs = {}
        if kwargs is not None:
            for key, value in kwargs.items():
                if value:
                    good_kwargs[key] = value
        return good_kwargs

    def search_list_by_keyword(self, **kwargs):
        # See full sample for function
        self.kwargs = self.remove_empty_kwargs(**kwargs)

        self.response = self.client.search().list(**kwargs).execute()
