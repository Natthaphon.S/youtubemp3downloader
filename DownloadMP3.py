import youtube_dl as dl
import Searcher
import Dowloader as DL
CLIENT_SECRETS_FILE = "client_secret.json"
SCOPES = ['https://www.googleapis.com/auth/youtube.force-ssl']
API_SERVICE_NAME = 'youtube'
API_VERSION = 'v3'


ydl = dl.YoutubeDL({'outtmpl': '%(id)s%(ext)s'})
searcher = Searcher.Search(CLIENT_SECRETS_FILE,SCOPES,API_SERVICE_NAME,API_VERSION)
result = []
select = []
selectName = []
"""
searcher.search_list_by_keyword(part='snippet',
                           maxResults=25,
                           q='Marshmallow',
                           type='video')
                           """
#print(searcher.response["items"])

def showresult():

    response = searcher.response
    serachlist = response["items"]
    global result
    result = []

    for i in range(len(serachlist)):
        bullet = serachlist[i]
        result.append({"no":i,"info":{"name":bullet["snippet"]["title"],"id":bullet['id']['videoId']}})
        print(i,"\t\t",bullet["snippet"]["title"])

while True:
    cmd = input("command : ")
    if cmd.startswith("s"):
        searcher.search_list_by_keyword(part='snippet',
                           maxResults=25,
                           q=cmd[2:].strip(),
                           type='video')
        showresult()
        #print(result)

    elif cmd.startswith("a"):
        try:
            num = int(cmd[2:].strip())
        except:
            print("please enter only number")
        if num not in range(25):
            print("number can be only 0 - 24")
        else:
            try:
                select.append(("https://www.youtube.com/watch?v="+result[num]["info"]["id"]))
                selectName.append(( result[num]["info"]["name"]))
            except IndexError:
                print("You haven't search yet")


    elif cmd.startswith("dl"):
        try:
            import Dowloader as DL
            for i in range(len(selectName)):
                print(i, "\t", selectName[i], "\t", select[i])
            DL.ydownload(select)
            select = []
            selectName =[]
        except:
            print("something went wrong")

    elif cmd == "d all":
        select = []
        selectName = []

    elif cmd.startswith("d"):
        try:
            dnum = int(cmd[2:].strip())
            del selectName[dnum]
            del select[dnum]
        except ValueError:
            print('enter number of song you want to delete')
        except:
            print("something went wrong with cmd : d dnum")



    elif cmd=="list":
        for i in range(len(selectName)):
            print(i,"\t",selectName[i],"\t",select[i])

    elif cmd.startswith("exit"):
        print("exit program")
        break

    else:
        print("unknown command!! ")